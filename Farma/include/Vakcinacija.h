#ifndef VAKCINACIJA_H
#define VAKCINACIJA_H
#include <string>
#include <iostream>
#include "Vakcina.h"
using namespace std;
class Vakcinacija
{

private:
    string datVakc;
    Vakcina* primljenaVakc;

public:
    Vakcinacija();
    void prikazVakcinacije();
    string getDatVakc();
    Vakcina* getPrimljenaVakc();
    void setDatVakc(string noviDat);
    void setPrimljenaVakc(Vakcina* sv);



};

#endif // VAKCINACIJA_H
