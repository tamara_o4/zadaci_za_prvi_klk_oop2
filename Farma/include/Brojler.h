#ifndef BROJLER_H
#define BROJLER_H

#include <Zivina.h>
#include <vector>
#include "Vakcinacija.h"

class Brojler : public Zivina
{
public:
    Brojler();
    void detalji();
    void setVakcinacije(Vakcinacija* vakcinacija);
    vector <Vakcinacija*> getVakcinacije();
    void vakcinisati(string datum, Vakcinacija* v);
    void karnonVakcinacija();


private:
    vector <Vakcinacija*> vakcinacije;
};

#endif // BROJLER_H
