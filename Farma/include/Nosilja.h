#ifndef NOSILJA_H
#define NOSILJA_H

#include <Zivina.h>


class Nosilja : public Zivina
{
public:
    Nosilja();
    void detalji();
    void setBrJaja(int sBrJaja);
    void setKvocka(bool sKvocka);
    int getBrJaja();
    bool getKvocka();

private:
    int brJaja;
    bool kvocka;
};

#endif // NOSILJA_H
