#ifndef FARMA_H
#define FARMA_H
#include <Zivina.h>
#include <vector>
#include <string>
#include <iostream>
using namespace std;

class Farma
{
public:
    Farma();
    void dodajJedinku(Zivina* novaJedinka);
    void prikazJedinki();

private:
    vector <Zivina*> sveJedinke;
};

#endif // FARMA_H
