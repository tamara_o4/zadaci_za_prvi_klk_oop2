#ifndef ZIVINA_H
#define ZIVINA_H
#include <string>
#include <iostream>
#include <sstream>
using namespace std;

class Zivina
{
public:
    Zivina();
    virtual void detalji()=0;
    void setSifraJedinke(string sSifraJedinke);
    void setMassa (double sMasa);
    void setNazivVrste (string sNazivVrste);
    string getSifraJedinke();
    double getMasa();
    string getNazivVrste();


private:
    string sifraJedninke;
    double masa;
    string nazivVrste;
};

#endif // ZIVINA_H
