#ifndef VAKCINA_H
#define VAKCINA_H
#include <iostream>
#include <string>
using namespace std;

class Vakcina
{
public:
    Vakcina();
    void prikaz();
    string getNazivVakc();
    string getProizvodjac();
    string getOpisVakc();
    void setNazivVakc(string sNazivVakc);
    void setProizvodjac(string sProizvodjac);
    void setOpisVakc(string sOpisVakc);

private:
    string nazivVakc;
    string proizvodjac;
    string opisVakc;
};

#endif // VAKCINA_H
