#include "Nosilja.h"

Nosilja::Nosilja()
{
   Zivina::setNazivVrste("nosilja");
}

void Nosilja::detalji(){
    Zivina::detalji();
    if (kvocka == true){
        cout << " Jeste kvocka; br jaja: " << brJaja;
    }
    else{
        cout << " Nije kvocka";
    }

};
void Nosilja::setBrJaja(int sBrJaja){
    brJaja +=sBrJaja;
};
void Nosilja::setKvocka(bool sKvocka){
    kvocka = sKvocka;
};
int Nosilja::getBrJaja(){
    return brJaja;
};
bool Nosilja::getKvocka(){
    return kvocka;
};
