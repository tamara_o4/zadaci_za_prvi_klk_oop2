#include <iostream>
#include "Zivina.h"
#include "Brojler.h"
#include "Nosilja.h"
#include "Farma.h"
#include "Vakcina.h"
#include "Vakcinacija.h"
using namespace std;

int main()
{
    cout << "-------- F A R M A ----------" << endl;
    Farma* f = new Farma();

    Vakcina* v1 = new Vakcina();
    v1->setNazivVakc("Lsikgm");
    v1->setProizvodjac("VetPet");
    v1->setOpisVakc("protiv besnila");

    Vakcinacija* vcij1 = new Vakcinacija();
    vcij1->setDatVakc("22.1.2019.");
    vcij1->setPrimljenaVakc(v1);


    Brojler* b1 = new Brojler();
    b1->setSifraJedinke("123");
    b1->setMassa(5);
    //b1->setVakcinacije("25.5.2019.");
    //b1->setVakcinacije("25.9.2019.");
    b1->setVakcinacije(vcij1);
    b1->karnonVakcinacija();

    Nosilja* n1 = new Nosilja();
    n1->setSifraJedinke("987");
    n1->setMassa(2);
    n1->setKvocka(false);

    Nosilja* n2 = new Nosilja();
    n2->setSifraJedinke("999");
    n2->setMassa(6);
    n2->setKvocka(true);
    n2->setBrJaja(20);
    n2->setBrJaja(15);

    f->dodajJedinku(b1);
    f->dodajJedinku(n1);
    f->dodajJedinku(n2);

    f->prikazJedinki();


/**
    b1->detalji();
    cout << endl;
    n1->detalji();
    cout << endl;
    n2->detalji();

*/
    return 0;
}
