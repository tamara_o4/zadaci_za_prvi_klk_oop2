#include <iostream>

#include "Zaposleni.h"
#include "Lekar.h"
#include "MedTeh.h"
#include "Klinika.h"
#include "Pacijent.h"

using namespace std;

int main()
{
    cout << "*** K L I N I K A ***\n\n\n" << endl;

    Klinika *k = new Klinika();

    MedTeh *mt = new MedTeh();
    mt->setIme("Pera");
    mt->setPrezime("Peric");
    mt->setDatRodj("21.05.1992.");
    mt->setSifra("CCC2525");
    mt->setTitula("asistent");

    Lekar *l = new Lekar();
    l->setIme("Sava");
    l->setPrezime("Ilic");
    l->setDatRodj("11.05.1982.");
    l->setSifra("SL5525");
    l->setTitula("stomatolog");

    Pacijent *p = new Pacijent();
    p->setIme("Pera");
    p->setPrezime("Peric");
    p->setSifraPacijenta("eeessiii");


    cout << endl;


//    mt->predstaviSe();
//    cout << endl;
//    l->predstaviSe();
    k->dodajZaposlnog(mt);
    k->dodajZaposlnog(l);
    k->prikazSvihZap();

//    p->predstaviSe();
    k->dodajPacijenta(p, "SL5525");
    k->prikazPacijenata();
    return 0;
}
