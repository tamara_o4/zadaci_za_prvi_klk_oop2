#ifndef LEK_H
#define LEK_H
#include<vector>
#include <string>
#include <iostream>
using namespace std;

class Lek
{
public:
    Lek();

private:
    string naziv;
    vector <string> simptomiIzaziva;
    vector <string> simptomiOtklanja;

};

#endif // LEK_H
