#ifndef OSOBA_H
#define OSOBA_H
#include <string>
#include <iostream>
using namespace std;

class Osoba{
public:
    Osoba();
    Osoba(string cIme, string cPrezime, string cDatRodj);
    void setIme(string sIme);
    void setPrezime(string sPrezime);
    void setDatRodj(string sDatRodj);
    string getIme();
    string getPrezime();
    string getDatRodj();
    virtual void predstaviSe()=0;

private:
    string ime;
    string prezime;
    string datRodj;
};

#endif // OSOBA_H
