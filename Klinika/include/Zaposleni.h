#ifndef ZAPOSLENI_H
#define ZAPOSLENI_H

#include <Osoba.h>


class Zaposleni : public Osoba{
public:
    Zaposleni();
    void setSifra(string sSifra);
    string getSifra();
    void predstaviSe();

private:
    string sifra;
};

#endif // ZAPOSLENI_H
