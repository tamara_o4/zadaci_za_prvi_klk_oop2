#ifndef MEDTEH_H
#define MEDTEH_H

#include <Zaposleni.h>


class MedTeh : public Zaposleni
{
public:
    MedTeh();
    void setTitula(string sTitula);
    string getTitula();
    void predstaviSe();

private:
    string titula;
};

#endif // MEDTEH_H
