#ifndef LEKAR_H
#define LEKAR_H

#include <Zaposleni.h>


class Lekar : public Zaposleni{
public:
    Lekar();
    void setTitula(string sTitula);
    string getTitula();
    void predstaviSe();

private:
    string titula;
};

#endif // LEKAR_H
