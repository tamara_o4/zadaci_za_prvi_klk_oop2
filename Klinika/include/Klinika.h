#ifndef KLINIKA_H
#define KLINIKA_H
#include <vector>
#include <string>
#include "Zaposleni.h"
#include "Pacijent.h"
#include "Lekar.h"
using namespace std;

class Klinika{
public:
    Klinika();
    void dodajZaposlnog(Zaposleni* noviZaposleni);
    void prikazSvihZap();
    void dodajPacijenta(Pacijent* p, string sifraLekara);
    void prikazPacijenata();

private:
    vector <Zaposleni*> listaZaposlenih;
    vector <Pacijent*> listaPacijenata;
};

#endif // KLINIKA_H
