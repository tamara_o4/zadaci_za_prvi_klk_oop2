#ifndef PACIJENT_H
#define PACIJENT_H

#include <Osoba.h>
#include <vector>
#include "Lekar.h"

class Pacijent : public Osoba{
public:
    Pacijent();
    void predstaviSe();
    void setSifraPacijenta (string sSifraPacijenta);
    string getSifraPacijenta();
    void setSimptomi(string simptom);
    vector <string> getSimptomi();
    void setIzabraniLekar(Lekar* sIzabraniLekar);
    Lekar* getIzabraniLekar();

private:
    string sifraPacijenta;
    vector <string> simptomi;
    Lekar* izabraniLekar;

};

#endif // PACIJENT_H
