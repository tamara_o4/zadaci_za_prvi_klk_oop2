#include "Osoba.h"

Osoba::Osoba(){

};

Osoba::Osoba(string cIme, string cPrezime, string cDatRodj){
    ime = cIme;
    prezime = cPrezime;
    datRodj = cDatRodj;
};
void Osoba::setIme(string sIme){
    ime = sIme;
};
void Osoba::setPrezime(string sPrezime){
    prezime = sPrezime;
};
void Osoba::setDatRodj(string sDatRodj){
    datRodj = sDatRodj;
};
string Osoba::getIme(){
    return ime;
};
string Osoba::getPrezime(){
    return prezime;
};
string Osoba::getDatRodj(){
    return datRodj;
};
void Osoba::predstaviSe(){
    cout << ime << " " << prezime;
};
