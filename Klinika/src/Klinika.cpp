#include "Klinika.h"

Klinika::Klinika(){
}

void Klinika::dodajZaposlnog(Zaposleni* noviZaposleni){
    listaZaposlenih.push_back(noviZaposleni);
};
void Klinika::prikazSvihZap(){
    cout << "Tabela svih zaposlenih\n";
    cout << "-----------------------\n";
    cout << "rbr | ime prezime, sifra, titula|\n";
    for(unsigned i = 0; i <listaZaposlenih.size(); i++){
        cout << i+1 << "   |";
        listaZaposlenih[i]->predstaviSe();
        cout << "|"  << endl;
    }
};

void Klinika::dodajPacijenta(Pacijent* p, string sifraLekara){
    Lekar* lekar;
    for(unsigned i = 0; i < listaZaposlenih.size(); i++){
        if ((*listaZaposlenih[i]).getSifra().compare(sifraLekara)==0){
            lekar = dynamic_cast<Lekar*>(listaZaposlenih[i]);
            break;
        }
    }
    p->setIzabraniLekar(lekar);
    listaPacijenata.push_back(p);
};

void Klinika::prikazPacijenata(){
    cout << "_______________________pacijenti__________________________"<<endl;
    for(unsigned i =0; i <listaPacijenata.size(); i++){
        cout<< i+1 << " | ";
        listaPacijenata[i]->predstaviSe();
        cout <<" |" << endl;
    };
};
