#include "Pacijent.h"

Pacijent::Pacijent(){

}
void Pacijent::setSifraPacijenta (string sSifraPacijenta){
    sifraPacijenta = sSifraPacijenta;
};
string Pacijent::getSifraPacijenta(){
    return sifraPacijenta;
};
void Pacijent::setSimptomi(string simptom){
    simptomi.push_back(simptom);
};
vector <string> Pacijent::getSimptomi(){
    return simptomi;
};

void Pacijent::setIzabraniLekar(Lekar* sIzabraniLekar){
    izabraniLekar = sIzabraniLekar;
};

Lekar* Pacijent::getIzabraniLekar(){
    return izabraniLekar;
};

void Pacijent ::predstaviSe(){
    Osoba ::predstaviSe();
    cout << " " <<sifraPacijenta << " " << izabraniLekar->getSifra();
};
